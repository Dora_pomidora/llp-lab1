#ifndef DATABASELLP_DATABASE_H
#define DATABASELLP_DATABASE_H

#include <stdio.h>
#include <sys/types.h>

#include "database_utils.h"

int create_database(const char* filename, size_t count, ...);

int open_database(const char* filename);

int close_database();

int delete_database(const char* filename);

off_t insert_database(size_t count, ...);

int select_database(size_t count, ...);

int erase_database_index(off_t pos);

int erase_database(size_t limit, size_t count, ...);

void set_stdout(FILE* value);

#endif  // DATABASELLP_DATABASE_H
