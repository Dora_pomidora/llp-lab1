#include "database.h"

#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "file_interface.h"
#include "header_utils.h"
#include "logger.h"

static file_header* header = NULL;

static FILE* stdout_fd;

int create_database(const char* filename, size_t count, ...) {
  int err_code = 0;
  va_list args;
  va_start(args, count);
  if (file_create(filename) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on creating file for a database with name %s.", filename);
    err_code = -1;
    goto end;
  }
  if ((header = create_header(filename, count, args)) == NULL) {
    file_close();
    err_code = -1;
    goto end;
  }
  if ((err_code = write_file_header(header)) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on writing file header of init database.");
    file_close();
    free(header);
    goto end;
  }
  stdout_fd = stdout;
end:
  va_end(args);
  return err_code;
}

int open_database(const char* filename) {
  if (file_open(filename) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on open database with name %s.", filename);
    return -1;
  }
  if ((header = read_file_header()) == NULL) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on reading a file-header.");
    return -1;
  }
  stdout_fd = stdout;
  return 0;
}

int close_database() {
  int err_code = 0;
  if (write_file_header(header) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on writing a file-header.");
    err_code = -1;
  }
  if (file_close() == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__, "Error on closing a database.");
    err_code = -1;
  }
  free(header);
  return err_code;
}

int delete_database(const char* filename) {
  if (file_delete(filename) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on deleting a database.");
    return -1;
  }
  return 0;
}

typedef struct {
  off_t pos;
  size_t size;
} pos_to_ins;

pos_to_ins* find_pos(size_t size) {
  pos_to_ins* result = malloc(sizeof(pos_to_ins));
  row* data = NULL;
  int err_code = 0;
  if (result == NULL) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for a result of find_pos.");
    return NULL;
  }
  if (header->next_free_block == 0) {
    result->size = 0;
    if ((result->pos = file_size()) == -1) {
      err_code = -1;
    }
    goto end;
  }
  if ((data = malloc(ROW_PADDING_SIZE)) == NULL) {
    err_code = -1;
    goto end;
  }
  if (read_from_pos(header->next_free_block, data, ROW_PADDING_SIZE) == -1) {
    err_code = -1;
    goto end;
  }
  if (data->size < size) {
    result->size = 0;
    if ((result->pos = file_size()) == -1) {
      err_code = -1;
    }
    goto end;
  }
  if (header->next_free_block == header->prev_free_block) {
    result->pos = header->next_free_block;
    result->size = data->size;
    header->prev_free_block = 0;
    header->next_free_block = 0;
    goto end;
  }
  result->pos = header->next_free_block;
  result->size = data->size;
  header->next_free_block = data->next;
  if (read_from_pos(header->next_free_block, data, ROW_PADDING_SIZE) == -1) {
    err_code = -1;
    goto end;
  }
  data->prev = 0;
  if (write_in_pos(header->next_free_block, data, ROW_PADDING_SIZE) == -1) {
    err_code = -1;
  }
end:
  if (data != NULL) {
    free(data);
  }
  if (err_code == -1) {
    free(result);
    result = NULL;
  }
  return result;
}

void left_header_link(off_t pos, row* data) {
  off_t data_offset = (off_t)ROW_PADDING_SIZE;
  off_t header_offset = (off_t)FILE_HEADER_PADDING_SIZE;
  column* cur_column = (column*)((char*)header + header_offset);
  char* cur_data = (char*)data + data_offset;
  header->next_data_block = pos;
  data->prev = 0;
  for (size_t i = 0; i < header->columns_cnt; ++i, ++cur_column) {
    element* cur_element = (element*)cur_data;
    cur_column->next = pos + data_offset;
    cur_element->prev = header_offset;
    header_offset += (off_t)sizeof(column);
    size_t offset_diff = ELEMENT_PADDING_SIZE + padding(cur_element->size);
    data_offset += (off_t)offset_diff;
    cur_data += offset_diff;
  }
}

void right_header_link(off_t pos, row* data) {
  off_t data_offset = (off_t)ROW_PADDING_SIZE;
  off_t header_offset = (off_t)FILE_HEADER_PADDING_SIZE;
  column* cur_column = (column*)((char*)header + header_offset);
  char* cur_data = (char*)data + data_offset;
  header->prev_data_block = pos;
  data->next = 0;
  for (size_t i = 0; i < header->columns_cnt; ++i, ++cur_column) {
    element* cur_element = (element*)cur_data;
    cur_column->prev = pos + data_offset;
    cur_element->next = header_offset;
    header_offset += (off_t)sizeof(column);
    size_t offset = ELEMENT_PADDING_SIZE + padding(cur_element->size);
    data_offset += (off_t)offset;
    cur_data += offset;
  }
}

void link_rows(off_t left_pos, row* left, off_t right_pos, row* right) {
  off_t left_offset = (off_t)ROW_PADDING_SIZE;
  off_t right_offset = (off_t)ROW_PADDING_SIZE;
  left->next = right_pos;
  right->prev = left_pos;
  char* cur_left = (char*)left + left_offset;
  char* cur_right = (char*)right + right_offset;
  for (size_t i = 0; i < header->columns_cnt; ++i) {
    element* left_element = (element*)cur_left;
    element* right_element = (element*)cur_right;
    left_element->next = right_pos + right_offset;
    right_element->prev = left_pos + left_offset;
    size_t cur_left_offset = ELEMENT_PADDING_SIZE + padding(left_element->size);
    size_t cur_right_offset =
        ELEMENT_PADDING_SIZE + padding(right_element->size);
    left_offset += (off_t)cur_left_offset;
    right_offset += (off_t)cur_right_offset;
    cur_left += cur_left_offset;
    cur_right += cur_right_offset;
  }
}

off_t insert_database(size_t count, ...) {
  if (count != header->columns_cnt) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "An incomparable number of columns (expected: %zu, found: %zu).",
           header->columns_cnt, count);
    return -1;
  }
  va_list args;
  va_start(args, count);
  row* data = create_row(header, args);
  pos_to_ins* res = find_pos(data->size);
  off_t pos = 0;
  if (res == NULL) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on finding position to insert a row.");
    pos = -1;
    goto end;
  }
  pos = res->pos;
  if (res->size != 0) {
    data->padding = res->size - data->size;
  }
  free(res);
  off_t prev = header->prev_data_block;
  right_header_link(pos, data);
  if (prev == 0) {
    left_header_link(pos, data);
  } else {
    row* prev_data;
    if ((prev_data = read_row(prev)) == NULL) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "Error on reading an previous row.");
      pos = -1;
      goto end;
    }
    link_rows(prev, prev_data, pos, data);
    if (write_row(prev, prev_data) == -1) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "Error on rewriting previous row to database.");
      free(prev_data);
      pos = -1;
      goto end;
    }
    free(prev_data);
  }
  if (write_row(pos, data) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on writing new row to database.");
    pos = -1;
    goto end;
  }
end:
  va_end(args);
  free(data);
  return pos;
}

void skip_list(COMPARE_TYPE compare_type, ELEMENT_TYPE element_type,
               va_list* args) {
  if (compare_type == JUST) {
    return;
  }
  switch (element_type) {
    case COLUMN_STRING:
      va_arg(*args, const char*);
      break;
    case COLUMN_DOUBLE:
      va_arg(*args, double);
      break;
    case COLUMN_INT32:
      va_arg(*args, int32_t);
      break;
    case COLUMN_BOOL:
      va_arg(*args, int);
      break;
  }
}

void fill_start_positions(off_t* positions, size_t count, va_list list) {
  va_list args;
  va_copy(args, list);
  for (size_t i = 0; i < count; ++i) {
    const char* str = va_arg(args, const char*);
    column* cur_column = (column*)((char*)header + FILE_HEADER_PADDING_SIZE);
    for (size_t j = 0; j < header->columns_cnt; ++j, ++cur_column) {
      if (!strcmp(cur_column->name, str)) {
        *(positions + i) = cur_column->next;
        break;
      }
    }
    skip_list(va_arg(args, int), cur_column->type, &args);
  }
}

int cmp_string(COMPARE_TYPE compare_type, const char* s1, const char* s2) {
  int cmp_value = strcmp(s1, s2);
  switch (compare_type) {
    case LESS:
      return cmp_value < 0;
    case LESS_EQ:
      return cmp_value <= 0;
    case EQ:
      return cmp_value == 0;
    case NOT_EQ:
      return cmp_value != 0;
    case GREATER_EQ:
      return cmp_value >= 0;
    case GREATER:
      return cmp_value > 0;
    default:
      return 1;
  }
}

#define CMP_TEMPLATE(_type)                                                    \
  int cmp_##_type(COMPARE_TYPE compare_type, const char* v1, const char* v2) { \
    _type lhs = *(const _type*)v1;                                             \
    _type rhs = *(const _type*)v2;                                             \
    switch (compare_type) {                                                    \
      case LESS:                                                               \
        return lhs < rhs;                                                      \
      case LESS_EQ:                                                            \
        return lhs <= rhs;                                                     \
      case EQ:                                                                 \
        return lhs == rhs;                                                     \
      case NOT_EQ:                                                             \
        return lhs != rhs;                                                     \
      case GREATER_EQ:                                                         \
        return lhs >= rhs;                                                     \
      case GREATER:                                                            \
        return lhs > rhs;                                                      \
      default:                                                                 \
        return 1;                                                              \
    }                                                                          \
  }

CMP_TEMPLATE(double)
CMP_TEMPLATE(int32_t)
CMP_TEMPLATE(char)

int validate_row(off_t* positions, size_t count, va_list list) {
  int err_code = 1;
  va_list args;
  va_copy(args, list);
  for (size_t i = 0; i < count; ++i, ++positions) {
    va_arg(args, const char*);
    COMPARE_TYPE compare_type = va_arg(args, int);
    if (compare_type == JUST) {
      continue;
    }
    element* data;
    if ((data = read_element(*positions, 1)) == NULL) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "Error on reading an element.");
      err_code = -1;
      break;
    }
    char* cur_data = (char*)data + ELEMENT_PADDING_SIZE;
    switch (data->type) {
      case COLUMN_STRING: {
        if (!cmp_string(compare_type, cur_data, va_arg(args, const char*))) {
          err_code = 0;
        }
        break;
      }
      case COLUMN_DOUBLE: {
        double value = va_arg(args, double);
        if (!cmp_double(compare_type, cur_data, (char*)&value)) {
          err_code = 0;
        }
        break;
      }
      case COLUMN_INT32: {
        int32_t value = va_arg(args, int32_t);
        if (!cmp_int32_t(compare_type, cur_data, (char*)&value)) {
          err_code = 0;
        }
        break;
      }
      case COLUMN_BOOL: {
        char value = va_arg(args, int) != 0;
        if (!cmp_char(compare_type, cur_data, &value)) {
          err_code = 0;
        }
        break;
      }
    }
    free(data);
    if (!err_code) {
      break;
    }
  }
  return err_code;
}

int column_print(const off_t* positions, size_t count) {
  int err_code = 0;
  for (size_t i = 0; i < count; ++i, ++positions) {
    element* data;
    if ((data = read_element(*positions, 1)) == NULL) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "Error on reading an element.");
      err_code = -1;
      break;
    }
    char* cur_data = (char*)data + ELEMENT_PADDING_SIZE;
    switch (data->type) {
      case COLUMN_STRING:
        fprintf(stdout_fd, "%s", cur_data);
        break;
      case COLUMN_DOUBLE:
        fprintf(stdout_fd, "%f", *(double*)cur_data);
        break;
      case COLUMN_INT32:
        fprintf(stdout_fd, "%d", *(int32_t*)cur_data);
        break;
      case COLUMN_BOOL:
        if (*cur_data == 0) {
          fprintf(stdout_fd, "false");
        } else {
          fprintf(stdout_fd, "true");
        }
        break;
    }
    free(data);
    if (i + 1 < count) {
      fprintf(stdout_fd, " | ");
    }
  }
  fprintf(stdout_fd, "\n");
  return err_code;
}

void header_print(size_t count, va_list list) {
  va_list args;
  va_copy(args, list);
  for (size_t i = 0; i < count; ++i) {
    const char* str = va_arg(args, const char*);
    column* cur_column = (column*)((char*)header + FILE_HEADER_PADDING_SIZE);
    for (size_t j = 0; j < header->columns_cnt; ++j, ++cur_column) {
      if (!strcmp(cur_column->name, str)) {
        switch (cur_column->type) {
          case COLUMN_STRING:
            fprintf(stdout_fd, "[STRING] ");
            break;
          case COLUMN_DOUBLE:
            fprintf(stdout_fd, "[DOUBLE] ");
            break;
          case COLUMN_INT32:
            fprintf(stdout_fd, "[INT32] ");
            break;
          case COLUMN_BOOL:
            fprintf(stdout_fd, "[BOOL] ");
            break;
        }
        fprintf(stdout_fd, "%s", str);
        break;
      }
    }
    skip_list(va_arg(args, int), cur_column->type, &args);
    if (i + 1 < count) {
      fprintf(stdout_fd, " | ");
    }
  }
  fprintf(stdout_fd, "\n");
  va_end(args);
}

int select_database(size_t count, ...) {
  off_t* positions;
  if ((positions = malloc(sizeof(off_t) * count)) == NULL) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for a positions in select.");
    return -1;
  }
  va_list args;
  va_start(args, count);
  header_print(count, args);
  int err_code = 0;
  fill_start_positions(positions, count, args);
  off_t cur_row = header->next_data_block;
  while (cur_row != 0) {
    int code = validate_row(positions, count, args);
    if (code == -1) {
      err_code = -1;
      break;
    }
    if (code == 1 && column_print(positions, count) == -1) {
      logger(ERROR, __FILE__, __func__, __LINE__, "Error on printing row.");
      err_code = -1;
      break;
    }
    for (size_t i = 0; i < count; ++i) {
      element* data;
      if ((data = read_element(*(positions + i), 0)) == NULL) {
        logger(ERROR, __FILE__, __func__, __LINE__,
               "Error on reading meta-data of an element.");
        err_code = -1;
        goto end;
      }
      *(positions + i) = data->next;
      free(data);
    }
    row* next_row;
    if ((next_row = malloc(ROW_PADDING_SIZE)) == NULL) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "Error on allocating memory for meta-data of a row.");
      err_code = -1;
      goto end;
    }
    if ((err_code = read_from_pos(cur_row, next_row, ROW_PADDING_SIZE)) == -1) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "Error on reading meta-data of a row.");
      free(next_row);
      err_code = -1;
      goto end;
    }
    cur_row = next_row->next;
    free(next_row);
  }
end:
  va_end(args);
  free(positions);
  return err_code;
}

int between_header(row* data) {
  if (data->prev != 0 || data->next != 0) {
    return -2;
  }
  header->prev_data_block = 0;
  header->next_data_block = 0;
  column* cur_column =
      (column*)(column*)((char*)header + FILE_HEADER_PADDING_SIZE);
  for (size_t i = 0; i < header->columns_cnt; ++i, ++cur_column) {
    cur_column->prev = 0;
    cur_column->next = 0;
  }
  return 0;
}

int between_rows(row* data) {
  if (data->prev == 0 || data->next == 0) {
    return -2;
  }
  int err_code = 0;
  row* prev = NULL;
  row* next = NULL;
  if ((prev = read_row(data->prev)) == NULL) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on reading previous row of deleting part.");
    err_code = -1;
    goto end;
  }
  if ((next = read_row(data->next)) == NULL) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on reading next row of deleting part.");
    err_code = -1;
    goto end;
  }
  link_rows(data->prev, prev, data->next, next);
  if ((err_code = write_row(data->prev, prev)) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on writing previous row of deleting part.");
    goto end;
  }
  if ((err_code = write_row(data->next, next)) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on writing next row of deleting part.");
    goto end;
  }
end:
  if (prev != NULL) {
    free(prev);
  }
  if (next != NULL) {
    free(next);
  }
  return err_code;
}

int previous_header(row* data) {
  if (data->prev != 0) {
    return -2;
  }
  int err_code = 0;
  row* next = NULL;
  if ((next = read_row(data->next)) == NULL) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on reading next row of deleting part.");
    err_code = -1;
    goto end;
  }
  left_header_link(data->next, next);
  if ((err_code = write_row(data->next, next)) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on writing next row of deleting part.");
    goto end;
  }

end:
  if (next != NULL) {
    free(next);
  }
  return err_code;
}

int next_header(row* data) {
  if (data->next != 0) {
    return -2;
  }
  int err_code = 0;
  row* prev = NULL;
  if ((prev = read_row(data->prev)) == NULL) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on reading previous row of deleting part.");
    err_code = -1;
    goto end;
  }
  right_header_link(data->prev, prev);
  if ((err_code = write_row(data->prev, prev)) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on writing previous row of deleting part.");
    goto end;
  }

end:
  if (prev != NULL) {
    free(prev);
  }
  return err_code;
}

off_t* find_free(size_t size) {
  off_t prev = 0;
  off_t current = header->next_free_block;
  while (current != 0) {
    row* data;
    if ((data = malloc(ROW_PADDING_SIZE)) == NULL) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "Error on reading meta-data of free block.");
      return NULL;
    }
    if (read_from_pos(current, data, ROW_PADDING_SIZE) == -1) {
      free(data);
      return NULL;
    }
    if (data->size <= size) {
      free(data);
      break;
    }
    prev = current;
    current = data->next;
  }
  off_t* result;
  if ((result = malloc(2 * sizeof(off_t))) == NULL) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for a result of free blocks.");
    return NULL;
  }
  *result = prev;
  *(result + 1) = current;
  return result;
}

int insert_free(const off_t* positions, off_t pos, row* data) {
  if (*positions == 0 && *(positions + 1) == 0) {
    header->next_free_block = pos;
    header->prev_free_block = pos;
    data->prev = 0;
    data->next = 0;
    return 0;
  }
  if (*positions == 0) {
    row* next;
    if ((next = malloc(ROW_PADDING_SIZE)) == NULL) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "Error on allocating memory for a meta-data of row.");
      return -1;
    }
    if (read_from_pos(*(positions + 1), next, ROW_PADDING_SIZE) == -1) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "Error on reading meta-data of a row.");
      free(next);
      return -1;
    }
    data->prev = 0;
    data->next = *(positions + 1);
    next->prev = pos;
    header->next_free_block = pos;
    if (write_in_pos(*(positions + 1), next, ROW_PADDING_SIZE) == -1) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "Error on writing meta-data of a row.");
      free(next);
      return -1;
    }
    free(next);
    return 0;
  }
  if (*(positions + 1) == 0) {
    row* prev;
    if ((prev = malloc(ROW_PADDING_SIZE)) == NULL) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "Error on allocating memory for a meta-data of row.");
      return -1;
    }
    if (read_from_pos(*positions, prev, ROW_PADDING_SIZE) == -1) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "Error on reading meta-data of a row.");
      free(prev);
      return -1;
    }
    data->prev = *positions;
    data->next = 0;
    header->prev_free_block = pos;
    prev->next = pos;
    if (write_in_pos(*positions, prev, ROW_PADDING_SIZE) == -1) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "Error on writing meta-data of a row.");
      free(prev);
      return -1;
    }
    free(prev);
    return 0;
  }
  row* prev = NULL;
  row* next = NULL;
  int err_code = 0;
  if ((prev = malloc(ROW_PADDING_SIZE)) == NULL) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for a meta-data of row.");
    err_code = -1;
    goto end;
  }
  if ((next = malloc(ROW_PADDING_SIZE)) == NULL) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for a meta-data of row.");
    err_code = -1;
    goto end;
  }
  if (read_from_pos(*positions, prev, ROW_PADDING_SIZE) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on reading meta-data of a row.");
    err_code = -1;
    goto end;
  }
  if (read_from_pos(*(positions + 1), next, ROW_PADDING_SIZE) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on reading meta-data of a row.");
    err_code = -1;
    goto end;
  }
  next->prev = pos;
  prev->next = pos;
  data->prev = *positions;
  data->next = *(positions + 1);
  if (write_in_pos(*(positions + 1), prev, ROW_PADDING_SIZE) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on writing meta-data of a row.");
    err_code = -1;
    goto end;
  }
  if (write_in_pos(*(positions + 1), next, ROW_PADDING_SIZE) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on writing meta-data of a row.");
    err_code = -1;
    goto end;
  }

end:
  if (prev != NULL) {
    free(prev);
  }
  if (next != NULL) {
    free(next);
  }
  return err_code;
}

int erase_database_index(off_t pos) {
  row* data;
  if ((data = read_row(pos)) == NULL) {
    return -1;
  }
  int err_code = between_header(data);
  if (err_code == 0) {
    goto end;
  }
  err_code = previous_header(data);
  if (err_code == -1 || err_code == 0) {
    goto end;
  }
  err_code = next_header(data);
  if (err_code == -1 || err_code == 0) {
    goto end;
  }
  err_code = between_rows(data);
  if (err_code == -1 || err_code == 0) {
    goto end;
  }

end:
  if (err_code == 0) {
    data->size += data->padding;
    data->padding = 0;
    off_t* free_positions = find_free(data->size);
    if ((err_code = insert_free(free_positions, pos, data)) == -1) {
      free(free_positions);
      goto rtn;
    }
    if ((err_code = write_in_pos(pos, data, ROW_PADDING_SIZE)) == -1) {
      free(free_positions);
      goto rtn;
    }
    free(free_positions);
  }
rtn:
  free(data);
  return err_code;
}

int next_rows(off_t* positions, size_t count) {
  int err_code = 0;
  for (size_t i = 0; i < count; ++i, ++positions) {
    element* data;
    if ((data = read_element(*positions, 0)) == NULL) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "Error on reading a meta-data of an element.");
      err_code = -1;
      goto end;
    }
    *(positions + i) = data->next;
    free(data);
  }
end:
  return err_code;
}

int erase_database(size_t limit, size_t count, ...) {
  off_t* positions;
  if ((positions = malloc(sizeof(off_t) * count)) == NULL) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for a positions in select.");
    return -1;
  }
  va_list args;
  va_start(args, count);
  int err_code = 0;
  fill_start_positions(positions, count, args);
  off_t cur_row = header->next_data_block;
  while (cur_row != 0 && limit != 0) {
    int code = validate_row(positions, count, args);
    if (code == -1) {
      err_code = -1;
      break;
    }
    row* next_row;
    if ((next_row = malloc(ROW_PADDING_SIZE)) == NULL) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "Error on allocating memory for meta-data of a row.");
      err_code = -1;
      goto end;
    }
    if ((err_code = read_from_pos(cur_row, next_row, ROW_PADDING_SIZE)) == -1) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "Error on reading meta-data of a row.");
      free(next_row);
      goto end;
    }
    off_t next_position = next_row->next;
    free(next_row);
    if (next_rows(positions, count) == -1) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "Error on get next positions of elements.");
      err_code = -1;
      goto end;
    }
    if (code == 1 && erase_database_index(cur_row) == -1) {
      err_code = -1;
      goto end;
    }
    if (code == 1) {
      --limit;
    }
    cur_row = next_position;
  }
end:
  va_end(args);
  free(positions);
  return err_code;
}

void set_stdout(FILE* value) { stdout_fd = value; }
