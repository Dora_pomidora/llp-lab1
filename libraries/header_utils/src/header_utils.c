#include "header_utils.h"

#include <memory.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "file_interface.h"
#include "logger.h"

size_t padding(size_t size) {
  if (size % CHUNK_SIZE == 0) {
    return size;
  }
  return size - (size % CHUNK_SIZE) + CHUNK_SIZE;
}

file_header* create_header(const char* filename, size_t count, va_list list) {
  va_list args;
  va_copy(args, list);
  size_t size = FILE_HEADER_PADDING_SIZE + count * COLUMN_PADDING_SIZE;
  file_header* header;
  if ((header = malloc(size)) == NULL) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for a file header.");
    goto end;
  }
  memset(header, 0, size);
  header->size = size;
  header->columns_cnt = count;
  strcpy(header->name, filename);
  column* cur_column = (column*)((char*)header + FILE_HEADER_PADDING_SIZE);
  for (size_t i = 0; i < count; ++i, ++cur_column) {
    cur_column->type = va_arg(args, int);
    const char* str = va_arg(args, const char*);
    strcpy(cur_column->name, str);
  }

end:
  va_end(args);
  return header;
}

file_header* read_file_header() {
  file_header* header;
  if ((header = malloc(FILE_HEADER_PADDING_SIZE)) == NULL) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for meta-data of file header.");
    return NULL;
  }
  if (read_from_pos(0, header, FILE_HEADER_PADDING_SIZE) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on reading meta-data of file-header.");
    free(header);
    return NULL;
  }
  size_t size = header->size;
  free(header);
  if ((header = malloc(size)) == NULL) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for file-header.");
    return NULL;
  }
  if (read_from_pos(0, header, size) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on reading file-header.");
    free(header);
    return NULL;
  }
  return header;
}

int write_file_header(const file_header* header) {
  if (write_in_pos(0, header, header->size) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on writing file-header.");
    return -1;
  }
  return 0;
}

size_t calculate_row_size(const file_header* header, va_list list) {
  va_list args;
  va_copy(args, list);
  size_t size = ROW_PADDING_SIZE;
  column* cur_column = (column*)((char*)header + FILE_HEADER_PADDING_SIZE);
  for (size_t i = 0; i < header->columns_cnt; ++i, ++cur_column) {
    size_t element_size = ELEMENT_PADDING_SIZE;
    switch (cur_column->type) {
      case COLUMN_STRING: {
        const char* str = va_arg(args, const char*);
        element_size += strlen(str) + 1;
        break;
      }
      case COLUMN_DOUBLE: {
        double value = va_arg(args, double);
        element_size += sizeof(value);
        break;
      }
      case COLUMN_INT32: {
        int32_t value = va_arg(args, int32_t);
        element_size += sizeof(value);
        break;
      }
      case COLUMN_BOOL: {
        element_size += sizeof(char);
        char value = va_arg(args, int);
        break;
      }
    }
    size += padding(element_size);
  }
  va_end(args);
  return size;
}

row* create_row(const file_header* header, va_list list) {
  va_list args;
  va_copy(args, list);
  size_t size = calculate_row_size(header, list);
  row* data;
  if ((data = malloc(size)) == NULL) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for a row.");
    return NULL;
  }
  memset(data, 0, size);
  data->size = size;
  column* cur_column = (column*)((char*)header + FILE_HEADER_PADDING_SIZE);
  char* cur_data = (char*)data + ROW_PADDING_SIZE;
  for (size_t i = 0; i < header->columns_cnt; ++i, ++cur_column) {
    element* cur_element = (element*)cur_data;
    char* data_start = cur_data + ELEMENT_PADDING_SIZE;
    cur_element->type = cur_column->type;
    switch (cur_column->type) {
      case COLUMN_STRING: {
        const char* str = va_arg(args, const char*);
        cur_element->size = strlen(str) + 1;
        memcpy(data_start, str, cur_element->size);
        break;
      }
      case COLUMN_DOUBLE: {
        double value = va_arg(args, double);
        cur_element->size = sizeof(double);
        memcpy(data_start, &value, cur_element->size);
        break;
      }
      case COLUMN_INT32: {
        int32_t value = va_arg(args, int32_t);
        cur_element->size = sizeof(int32_t);
        memcpy(data_start, &value, cur_element->size);
        break;
      }
      case COLUMN_BOOL: {
        char value = va_arg(args, int) != 0;
        cur_element->size = sizeof(char);
        memcpy(data_start, &value, cur_element->size);
        break;
      }
    }
    cur_data += ELEMENT_PADDING_SIZE + padding(cur_element->size);
  }
  va_end(args);
  return data;
}

row* read_row(off_t pos) {
  row* data;
  if ((data = malloc(ROW_PADDING_SIZE)) == NULL) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for a row meta-data.");
    return NULL;
  }
  if (read_from_pos(pos, data, ROW_PADDING_SIZE) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on reading meta-data of a row.");
    free(data);
    return NULL;
  }
  size_t size = data->size + data->padding;
  free(data);
  if ((data = malloc(size)) == NULL) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for a row.");
    return NULL;
  }
  if (read_from_pos(pos, data, size) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on reading data of a row.");
    free(data);
    return NULL;
  }
  return data;
}

int write_row(off_t pos, const row* data) {
  if (write_in_pos(pos, data, data->size + data->padding) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__, "Error on writing an a row.");
    return -1;
  }
  return 0;
}

element* read_element(off_t pos, char type) {
  element* data;
  if ((data = malloc(ELEMENT_PADDING_SIZE)) == NULL) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for meta-data of an element.");
    return NULL;
  }
  if (read_from_pos(pos, data, ELEMENT_PADDING_SIZE) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on reading meta-data of an element.");
    free(data);
    return NULL;
  }
  if (!type) {
    return data;
  }
  size_t size = ELEMENT_PADDING_SIZE + padding(data->size);
  free(data);
  if ((data = malloc(size)) == NULL) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for an element.");
    return NULL;
  }
  if (read_from_pos(pos, data, size) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__, "Error on reading an element.");
    free(data);
    return NULL;
  }
  return data;
}

int write_element(off_t pos, const element* data, char type) {
  size_t size = ELEMENT_PADDING_SIZE;
  if (type) {
    size += padding(data->size);
  }
  if (write_in_pos(pos, data, size) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__, "Error on writing an element.");
    return -1;
  }
  return 0;
}
