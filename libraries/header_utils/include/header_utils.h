#ifndef DATABASELLP_HEADER_UTILS_H
#define DATABASELLP_HEADER_UTILS_H

#include <stdarg.h>
#include <sys/types.h>

#include "database_utils.h"

#define NAME_LENGTH 256

typedef struct {
  size_t size;
  off_t prev;
  off_t next;
  char type;
  char data[];
} element;

typedef struct {
  off_t prev;
  off_t next;
  char type;
  char name[NAME_LENGTH];
} column;

typedef struct {
  size_t size;
  size_t padding;
  off_t prev;
  off_t next;
  char data[];
} row;

typedef struct {
  size_t size;
  char name[NAME_LENGTH];
  size_t columns_cnt;
  off_t prev_data_block;
  off_t next_data_block;
  off_t prev_free_block;
  off_t next_free_block;
  char data[];
} file_header;

size_t padding(size_t size);

file_header* create_header(const char* filename, size_t count, va_list list);

file_header* read_file_header();

int write_file_header(const file_header* header);

row* create_row(const file_header* header, va_list list);

row* read_row(off_t pos);

int write_row(off_t pos, const row* data);

element* read_element(off_t pos, char type);

int write_element(off_t pos, const element* data, char type);

#define ROW_PADDING_SIZE (padding(sizeof(row)))

#define ELEMENT_PADDING_SIZE (padding(sizeof(element)))

#define FILE_HEADER_PADDING_SIZE (padding(sizeof(file_header)))

#define COLUMN_PADDING_SIZE (padding(sizeof(column)))

#endif  // DATABASELLP_HEADER_UTILS_H
