project(database_utils)

add_library(${PROJECT_NAME} include/${PROJECT_NAME}.h src/${PROJECT_NAME}.c)

target_include_directories(${PROJECT_NAME} PUBLIC include)

target_link_libraries(${PROJECT_NAME} PRIVATE file_interface logger)

add_library(DatabaseLLP::${PROJECT_NAME} ALIAS ${PROJECT_NAME})
