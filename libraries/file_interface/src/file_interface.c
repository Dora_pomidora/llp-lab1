#include "file_interface.h"

#include <memory.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <unistd.h>

#include "logger.h"

const static mode_t read_mode = S_IRUSR;
const static mode_t write_mode = S_IWUSR;

#if defined(_WIN32) || defined(_WIN64)
// _O_BINARY this solving: \n -> \r\n
const static int open_flags = O_CREAT | O_TRUNC | O_RDWR | _O_BINARY;
const static int write_flags = O_WRONLY | _O_BINARY;
const static int read_flags = O_RDONLY | _O_BINARY;
#else
const static int open_flags = O_CREAT | O_TRUNC | O_RDWR;
const static int write_flags = O_WRONLY;
const static int read_flags = O_RDONLY;
#endif

static int read_descriptor = -1;
static int write_descriptor = -1;

int file_create(const char *filename) {
  if ((write_descriptor = open(filename, open_flags, write_mode | read_mode)) ==
          -1 ||
      (read_descriptor = open(filename, read_flags, read_mode)) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Can not create a file with name %s.", filename);
    file_close();
    return -1;
  }
  return 0;
}

int file_open(const char *filename) {
  if ((read_descriptor = open(filename, read_flags, read_mode)) == -1 ||
      (write_descriptor = open(filename, write_flags, write_mode)) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Can not open a file with name %s.", filename);
    return -1;
  }
  return 0;
}

int file_close() {
  int err_code = 0;
  if (read_descriptor == -1 || (err_code = close(read_descriptor)) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Invalid read file descriptor or can not close read fd.");
  }
  if (write_descriptor == -1 || (err_code = close(write_descriptor)) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Invalid write file descriptor or can not close write fd.");
  }
  return err_code;
}

int file_delete(const char *filename) {
  if (unlink(filename) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Can not delete a file with name %s.", filename);
    return -1;
  }
  return 0;
}

int file_write(const void *buf, size_t size) {
  const static char *message[] = {
      "Can not write to the fd.",
      "Write only %zu bytes (expected %zu) to the fd."};
  ssize_t count = write(write_descriptor, buf, size);
  if (count == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__, message[0]);
    return -1;
  }
  if (count < size) {
    logger(WARNING, __FILE__, __func__, __LINE__, message[1], count, size);
  }
  return 0;
}

ssize_t file_read(void *buf, size_t size) {
  const static char *message[] = {
      "Can not read from the fd.",
      "Read only %zu bytes (expected %zu) from the fd."};
  ssize_t count = read(read_descriptor, buf, size);
  if (count == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__, message[0]);
    return -1;
  }
  if (count < size) {
    logger(WARNING, __FILE__, __func__, __LINE__, message[1], count, size);
  }

  return count;
}

int write_move(off_t pos) {
  if (lseek(write_descriptor, pos, SEEK_SET) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on moving read file pointer to pos %lld.", pos);
    return -1;
  }
  return 0;
}

int read_move(off_t pos) {
  if (lseek(read_descriptor, pos, SEEK_SET) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Error on moving read file pointer to pos %lld.", pos);
    return -1;
  }
  return 0;
}

off_t file_size() {
  off_t result;
  if ((result = lseek(write_descriptor, 0, SEEK_END)) == -1) {
    logger(ERROR, __FILE__, __func__, __LINE__, "Invalid fd or an error.");
    return -1;
  }
  return result;
}

struct {
  char data[CHUNK_SIZE];
  off_t offset;
} write_buffer;

int write_in_pos(off_t pos, const void *buf, size_t size) {
  if (pos % CHUNK_SIZE != 0 || size % CHUNK_SIZE != 0) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Offset must be divided by CHUNK_SIZE (32).");
    return -1;
  }
  if (pos != write_buffer.offset) {
    if (write_move(pos) == -1) {
      return -1;
    }
    write_buffer.offset = pos;
  }
  size_t i = 0;
  for (; i < size; i += CHUNK_SIZE, write_buffer.offset += CHUNK_SIZE) {
    memmove(write_buffer.data, buf, CHUNK_SIZE);
    if (file_write(write_buffer.data, CHUNK_SIZE) == -1) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "Error on writing next chunk of bytes.");
      return -1;
    }
    buf = (char *)buf + CHUNK_SIZE;
  }
  return 0;
}

struct {
  char data[CHUNK_SIZE];
  off_t offset;
  size_t size;
} read_buffer;

int read_from_pos(off_t pos, void *buf, size_t size) {
  if (pos % CHUNK_SIZE != 0 || size % CHUNK_SIZE != 0) {
    logger(ERROR, __FILE__, __func__, __LINE__,
           "Offset must be divided by CHUNK_SIZE (32).");
    return -1;
  }
  if (pos != read_buffer.offset) {
    if (read_move(pos) == -1) {
      return -1;
    }
    read_buffer.size = 0;
    read_buffer.offset = pos;
  }
  size_t i = 0;
  for (; i < size; i += CHUNK_SIZE, read_buffer.offset += CHUNK_SIZE) {
    if ((read_buffer.size = file_read(read_buffer.data, CHUNK_SIZE)) == -1) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "Error on reading next chunk of bytes.");
      return -1;
    }
    memmove((char *)buf + i, read_buffer.data, read_buffer.size);
  }
  return 0;
}
