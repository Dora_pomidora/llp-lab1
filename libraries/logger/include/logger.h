#ifndef DATABASELLP_LOGGER_H
#define DATABASELLP_LOGGER_H

#include <stddef.h>

typedef enum {
  DEBUG,
  WARNING,
  ERROR,
} LOG_TYPE;

void logger(LOG_TYPE type, const char *file, const char *function, int line,
            const char *fmt, ...);

#endif  // DATABASELLP_LOGGER_H
