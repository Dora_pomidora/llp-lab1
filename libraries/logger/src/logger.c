#include "logger.h"

#include <stdarg.h>
#include <stdio.h>

static const char *message[] = {
    [DEBUG] = "[debug]", [WARNING] = "[warn]", [ERROR] = "[error]"};

void logger(LOG_TYPE type, const char *file, const char *function, int line,
            const char *fmt, ...) {
  va_list args;
  va_start(args, fmt);
  fprintf(stderr, "%s %s:%s:%d : ", message[type], file, function, line);
  vfprintf(stderr, fmt, args);
  fprintf(stderr, "\n");
  va_end(args);
}
