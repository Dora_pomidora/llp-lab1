#include <database.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include "logger.h"

const static char insert_erase_db[] = "test_insert_erase";
const static char select_db[] = "select_db";
const static char epochs_db[] = "epochs";
const static char inserts[] = "only_inserts";

#define CLOCKS_PER_MS (CLOCKS_PER_SEC / 1000)

static FILE* fd;

#define THROW(expr)   \
  do {                \
    if ((expr) < 0) { \
      return -1;      \
    }                 \
  } while (0)

void delete_value(int32_t* arr, size_t pos, size_t* size) {
  for (size_t i = pos; i + 1 < *size; ++i) {
    arr[i] = arr[i + 1];
  }
  --*size;
}

int insert_erase_test(size_t iteration_count, int mode) {
  THROW(create_database(insert_erase_db, 5, COLUMN_STRING, "FIRST COLUMN",
                        COLUMN_DOUBLE, "SECOND COLUMN", COLUMN_INT32,
                        "THIRD COLUMN", COLUMN_BOOL, "FOURTH COLUMN",
                        COLUMN_STRING, "FIFTH COLUMN"));
  set_stdout(fd);
  const static size_t insert_count = 450;
  const static size_t erase_count = 350;
  for (size_t i = 0; i < iteration_count; ++i) {
    // insert values
    int32_t free_indexes[insert_count];
    if (mode & 1) {
      const clock_t start_insert = clock();
      for (size_t j = 0; j < insert_count; ++j) {
        free_indexes[j] = (int32_t)(insert_count * i) + (int32_t)j;
        THROW(insert_database(5, "First column is useless", 3.14,
                              free_indexes[j], j % 2,
                              "Last column is too useless, as first"));
      }
      const clock_t end_insert = clock();
      if (mode & 2) {
        printf("Insert iterations %zu performance: %f\n", i + 1,
               (double)(end_insert - start_insert) / CLOCKS_PER_MS);
      }
    }
    // erase values
    if (mode & 4) {
      srand(time(NULL));
      size_t erase_pos[erase_count];
      size_t free_indexes_size = insert_count;
      for (size_t j = 0; j < erase_count; ++j) {
        size_t deleted_pos = (size_t)rand() % free_indexes_size;
        erase_pos[j] = free_indexes[deleted_pos];
        delete_value(free_indexes, deleted_pos, &free_indexes_size);
      }
      const clock_t start_erase = clock();
      for (size_t j = 0; j < erase_count; ++j) {
        THROW(erase_database(1, 1, "THIRD COLUMN", EQ, erase_pos[j]));
      }
      const clock_t end_erase = clock();
      if (mode & 8) {
        printf("Erase iterations %zu performance: %f\n", i + 1,
               (double)(end_erase - start_erase) / CLOCKS_PER_MS);
      }
    }
  }
  THROW(select_database(5, "FIRST COLUMN", JUST, "SECOND COLUMN", JUST,
                        "THIRD COLUMN", JUST, "FOURTH COLUMN", JUST,
                        "FIFTH COLUMN", JUST));
  THROW(close_database());
  THROW(delete_database(insert_erase_db));
  return 0;
}

int inserts_test(size_t count) {
  THROW(create_database(inserts, 5, COLUMN_STRING, "FIRST COLUMN",
                        COLUMN_DOUBLE, "SECOND COLUMN", COLUMN_INT32,
                        "THIRD COLUMN", COLUMN_BOOL, "FOURTH COLUMN",
                        COLUMN_STRING, "FIFTH COLUMN"));
  set_stdout(fd);
  for (size_t i = 0; i < count; ++i) {
    const clock_t start_insert = clock();
    THROW(insert_database(5, "First column is useless", 3.14, i + 1, i % 2,
                          "Last column is too useless, as first"));
    const clock_t end_insert = clock();
    printf("insert number %zu perfmoarmance: %f\n", i + 1,
           (double)(end_insert - start_insert) / CLOCKS_PER_MS);
  }
  THROW(select_database(5, "FIRST COLUMN", JUST, "SECOND COLUMN", JUST,
                        "THIRD COLUMN", JUST, "FOURTH COLUMN", JUST,
                        "FIFTH COLUMN", JUST));
  THROW(close_database());
  THROW(delete_database(inserts));
  return 0;
}

int select_db_test(size_t insert_count) {
  THROW(create_database(select_db, 5, COLUMN_STRING, "FIRST COLUMN",
                        COLUMN_DOUBLE, "SECOND COLUMN", COLUMN_INT32,
                        "THIRD COLUMN", COLUMN_BOOL, "FOURTH COLUMN",
                        COLUMN_STRING, "FIFTH COLUMN"));
  set_stdout(fd);
  for (size_t i = 0; i < insert_count; ++i) {
    THROW(insert_database(5, "First column is useless", 3.14, i + 1, i % 2,
                          "Last column is too useless, as first"));
  }
  for (size_t i = 0; i < insert_count; ++i) {
    const clock_t select_start = clock();
    THROW(select_database(2, "FIRST COLUMN", JUST, "THIRD COLUMN", LESS_EQ,
                          i + 1));
    const clock_t select_end = clock();
    printf("select iteration %zu perfomance: %f\n", i + 1,
           (double)(select_end - select_start) / CLOCKS_PER_MS);
    fprintf(fd, "\n");
  }
  THROW(close_database());
  THROW(delete_database(select_db));
  return 0;
}

int first_epoch() {
  THROW(create_database(epochs_db, 5, COLUMN_STRING, "1", COLUMN_DOUBLE, "3.14",
                        COLUMN_STRING, "3", COLUMN_STRING, "4", COLUMN_STRING,
                        "LAST COLUMN"));
  for (size_t i = 0; i < 10000; ++i) {
    THROW(insert_database(5, "DALLVORO2023", 0.1 + 0.2, "SIMON", "SPB",
                          "VORONINA"));
  }
  THROW(close_database());
  return 0;
}

int second_epoch() {
  THROW(open_database(epochs_db));
  THROW(erase_database(500, 1, "DALLVORO2023", JUST));
  THROW(close_database());
  return 0;
}

int third_epoch() {
  THROW(open_database(epochs_db));
  for (size_t i = 0; i < 500; ++i) {
    THROW(insert_database(5, "DALLVORO2023", 0.1 + 0.2, "SIMON", "SPB",
                          "VORONINA"));
  }
  THROW(close_database());
  return 0;
}

int main(int argc, char** argv) {
  fd = fopen("test_output_for_database_output.txt", "wb");
  int err_code = 0;
  if (argc < 2) {
    logger(ERROR, __FILE__, __func__, __LINE__, "No such arguments.");
    err_code = -1;
    goto end;
  }
  if (!strcmp(argv[1], "inserts")) {
    if (argc < 3) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "There are not enough parameters for select_db_test, the count "
             "of inserts is required.");
      err_code = -1;
      goto end;
    }
    size_t iterations = (size_t)atoi(argv[2]);
    if (inserts_test(iterations) == -1) {
      logger(ERROR, __FILE__, __func__, __LINE__, "Failed test.");
      err_code = -1;
    }
    goto end;
  }
  if (!strcmp(argv[1], "insert_erase")) {
    if (argc < 4) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "There are not enough parameters for insert_erase_test, the "
             "number of iterations is "
             "required and mode of test.");
      err_code = -1;
      goto end;
    }
    int mode = 0;
    for (size_t i = 0; i < strlen(argv[3]); ++i) {
      switch (argv[3][i]) {
        case 'i':
          mode |= 1;
          break;
        case '1':
          mode |= 2;
          break;
        case 'r':
          mode |= 4;
          break;
        case '2':
          mode |= 8;
          break;
      }
    }
    size_t iterations = (size_t)atoi(argv[2]);
    clock_t start = clock();
    if (insert_erase_test(iterations, mode) == -1) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "The test failed with an error.");
      err_code = -1;
      goto end;
    }
    clock_t end = clock();
    printf("total time: %f", (double)(end - start) / CLOCKS_PER_MS);
    goto end;
  }
  if (!strcmp(argv[1], "selects")) {
    if (argc < 3) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "There are not enough parameters for select_db_test, the number "
             "of inserts is required.");
      err_code = -1;
      goto end;
    }
    size_t insert_count = (size_t)atoi(argv[2]);
    clock_t start = clock();
    if (select_db_test(insert_count) == -1) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "The test failed with an error.");
      err_code = -1;
      goto end;
    }
    clock_t end = clock();
    printf("total time: %f", (double)(end - start) / CLOCKS_PER_MS);
    goto end;
  }
  if (!strcmp(argv[1], "epochs")) {
    if (argc < 3) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "There are not enough parameters for select_db_test, the number "
             "of epoch is required.");
      err_code = -1;
      goto end;
    }
    size_t insert_count = (size_t)atoi(argv[2]);
    int local_err = 0;
    switch (insert_count) {
      case 1:
        local_err = first_epoch();
        break;
      case 2:
        local_err = second_epoch();
        break;
      case 3:
        local_err = third_epoch();
        break;
      default:
        logger(ERROR, __FILE__, __func__, __LINE__, "Incorrect epoch number.");
        break;
    }
    if (local_err == -1) {
      logger(ERROR, __FILE__, __func__, __LINE__,
             "The test failed with an error.");
      err_code = -1;
      goto end;
    }
  }
end:
  fclose(fd);
  return err_code;
}
